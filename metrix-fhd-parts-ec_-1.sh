#!/bin/sh
#
echo ''
echo '************************************************************'
echo "**                         STARTED                        **"
echo '************************************************************'
echo "**                 Uploaded by: Haitham                   **"
echo "************************************************************"
echo ''
sleep 3s


wget -O /tmp/metrix-fhd-parts-ec.tar.gz "https://gitlab.com/hmeng80/addons/-/raw/main/metrix-fhd-parts-ec.tar.gz"

tar -xzf /tmp/*.tar.gz -C /

rm -r /tmp/metrix-fhd-parts-ec.tar.gz

sleep 2;
echo "############ RESTARTING... #################" 
init 4
sleep 2
init 3
exit 0