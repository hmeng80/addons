#!/bin/sh
#
echo ''
echo '************************************************************'
echo "**                         STARTED                        **"
echo '************************************************************'
echo "**                 Uploaded by: Haitham                   **"
echo "************************************************************"
echo ''
sleep 3s

wget -O /tmp/ipaudiopro_d_t.tar.gz "https://gitlab.com/hmeng80/addons/-/raw/main/ipaudiopro_d_t.tar.gz"

tar -xzf /tmp/*.tar.gz -C /

rm -r /tmp/ipaudiopro_d_t.tar.gz

sleep 2;