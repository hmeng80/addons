#!/bin/sh
#
echo ''
echo '************************************************************'
echo "**                         STARTED                        **"
echo '************************************************************'
echo "**                 Uploaded by: Haitham                   **"
echo "************************************************************"
echo ''
sleep 3s

wget -O /tmp/cron-epggrabber.tar.gz "https://gitlab.com/hmeng80/addons/-/raw/main/cron-epggrabber.tar.gz"

tar -xzf /tmp/*.tar.gz -C /

rm -r /tmp/cron-epggrabber.tar.gz

echo "00 19 * * * bash /usr/script/cron-epggrabber.sh" >> /etc/cron/crontabs/root
sleep 2;