#!/bin/sh

# Configuration
plugin="metrix-skinparts"
version="mnasr"
targz_file="$plugin-$version.tar.gz"
package="enigma2-plugin-skins-metrix-atv-fhd-icons"
temp_dir="/tmp"
url="https://gitlab.com/hmeng80/addons/-/raw/main/metrix_skinparts_mnas.tar.gz"
backup="/usr/share/enigma2/MetrixHD/backup"

check_and_remove_package() {
if [ -d /usr/share/enigma2/MetrixHD/skinparts/mnasr_mod ]; then
echo "> removing package please wait..."
sleep 3 
rm -rf /usr/share/enigma2/MetrixHD/skinparts/mnasr_mod > /dev/null 2>&1
rm -rf /usr/share/enigma2/MetrixHD/skinparts/mnasr_mod $backup > /dev/null 2>&1
rm -rf /usr/share/enigma2/MetrixHD/skinparts/xtraevent $backup > /dev/null 2>&1
rm -rf /usr/share/enigma2/MetrixHD/skinparts/PosterX $backup > /dev/null 2>&1
rm -rf /usr/share/enigma2/MetrixHD/skinparts/TimeShiftState $backup > /dev/null 2>&1
rm -rf /usr/lib/enigma2/python/Components/Converter/MetrixHDaccess.py $backup > /dev/null 2>&1
rm -rf /usr/lib/enigma2/python/Components/Converter/MetrixHDCaidInfo2.py $backup > /dev/null 2>&1
rm -rf /usr/lib/enigma2/python/Components/Converter/MetrixHDTemp.py $backup > /dev/null 2>&1
rm -rf /usr/lib/enigma2/python/Components/Renderer/MetrixHDPosterX.py $backup > /dev/null 2>&1
rm -rf /usr/lib/enigma2/python/Components/Renderer/MetrixHDPosterXDownloadThread.py $backup > /dev/null 2>&1

if [ -d $backup/example ]; then
rm -rf /usr/share/enigma2/MetrixHD/skinparts/example > /dev/null 2>&1
cp -rf $backup/example /usr/share/enigma2/MetrixHD/skinparts/ > /dev/null 2>&1
fi

echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Haitham          *"
echo "*******************************************"
sleep 3
exit 1
else
echo "" 
fi  }
check_and_remove_package

# Remove unnecessary files and folders
[ -d "/CONTROL" ] && rm -r /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1

#backup original metrix skinparts
if [ ! -d $backup ]; then
mkdir $backup
fi
cp -rf /usr/share/enigma2/MetrixHD/skinparts/example $backup

#check and install fhd icons
echo "> checking atv fhd icons package please wait ..."
sleep 3
deps=( "enigma2-plugin-skins-metrix-atv-fhd-icons" )
left=">>>>"
right="<<<<"
LINE1="---------------------------------------------------------"
LINE2="-------------------------------------------------------------------------------------"
if [ -f /etc/opkg/opkg.conf ]; then
  STATUS='/var/lib/opkg/status'
  OSTYPE='Opensource'
  OPKG='opkg update'
  OPKGINSTAL='opkg install'
elif [ -f /etc/apt/apt.conf ]; then
  STATUS='/var/lib/dpkg/status'
  OSTYPE='DreamOS'
  OPKG='apt-get update'
  OPKGINSTAL='apt-get install -y'
fi
install() {
  if ! grep -qs "Package: $1" "$STATUS"; then
    $OPKG >/dev/null 2>&1
    rm -rf /run/opkg.lock
    echo -e "> Need to install ${left} $1 ${right} please wait..."
    echo "$LINE2"
    sleep 0.8
    echo
    if [ "$OSTYPE" = "Opensource" ]; then
      $OPKGINSTAL "$1"
      sleep 1
      clear
    elif [ "$OSTYPE" = "DreamOS" ]; then
      $OPKGINSTAL "$1" -y
      sleep 1
      clear
    fi
  fi
}
for i in "${deps[@]}"; do
  install "$i"
done

#download & install package
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3
wget -O $temp_dir/$targz_file --no-check-certificate $url
tar -xzf $temp_dir/$targz_file -C /
extract=$?
rm -rf $temp_dir/$targz_file >/dev/null 2>&1

echo ''
if [ $extract -eq 0 ]; then
echo "> $plugin-$version package installed successfully"
sleep 3
echo "> Uploaded By Haitham "
else
echo "> $plugin-$version package installation failed"
sleep 3
fi
    