#!/bin/sh
#
echo ''
echo '************************************************************'
echo "**                         STARTED                        **"
echo '************************************************************'
echo "**                 Uploaded by: Haitham                   **"
echo "**  https://www.tunisia-sat.com/forums/threads/4220254/   **"
echo "************************************************************"
echo ''
sleep 3s

wget -O /usr/lib/enigma2/python/Plugins/Extensions/MultiStalkerPro/icon.png "https://gitlab.com/hmeng80/addons/-/raw/main/icon.png"
wget -O /usr/lib/enigma2/python/Plugins/Extensions/IPaudioPro/logo.png "https://gitlab.com/hmeng80/addons/-/raw/main/logo.png"

echo ""
cd ..
sync
echo "############ INSTALLATION COMPLETED ########"
echo "############ RESTARTING... #################" 
init 4
sleep 2s
init 3
exit 0
